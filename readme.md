Валерия Можарова, Олег Овласюк

файл train_mr.py - map reducer для получения тренировочных данных
файл unknown_mr.py - map reducer для получения данных для предсказания
в файлах train_data.txt и predict_data.txt - полученные данные

Классификаторы:
classifier1.py - автор Валерия Можарова
classifier2.py - автор Олег Овласюк

результаты классификации пользователей из tv_uknown.txt в файле predicted_data.txt