from sklearn.feature_extraction import DictVectorizer
from sklearn.ensemble import *
from sklearn import cross_validation
from sklearn.svm import SVC

__author__ = "Valerie Mozharova"

def learn_classifier (samples, labels):
    clf = GradientBoostingClassifier(verbose = True)
    #clf = AdaBoostClassifier()
    clf = clf.fit(samples, labels)
    return clf

def classify (clf, features):
    print("classify")
    labels = clf.predict(features)
    return labels

def test_classifier(train_features, target):
    X_train, X_test, y_train, y_test = cross_validation.train_test_split(
    train_features, target, test_size=0.3, random_state=0)
    clf = learn_classifier(samples = X_train, labels = y_train)
    labels = classify(clf = clf, features = X_test)
    good = 0
    ln = 0
    print "len_labels = " + str(len(labels))
    for i in range(len(labels)):
        if y_test[i] == '2':
            continue;
        if labels[i] == y_test[i]:
           good += 1
        ln += 1
    return good * 1.0 / ln

def parser_train_data(train_file):
    p = []
    f = open(train_file, 'r+')
    for line in f:
        s = line.split("\t")
        item0 = eval(s[0])
        item1 = eval(s[1])
        p.append({"id": item0[0],
                  "label": item0[1],
                  "gender": item1.get(0),
                  "age": item1.get(1),
                  "os": item1.get(2),
                  "browser": item1.get(3),
                  "domain": item1.get(4) })

    f.close()
    return p

def parser_predict_data(predict_file):
    p = []
    f = open(predict_file, 'r+')
    for line in f:
        s = line.split("\t")
        item0 = eval(s[0])
        item1 = eval(s[1])
        p.append({"id": item0,
                  "gender": item1.get(0),
                  "age": item1.get(1),
                  "os": item1.get(2),
                  "browser": item1.get(3),
                  "domain": item1.get(4) })
    f.close()
    return p

def save_results(ids, labels, res_file):
    f = open(res_file, "w+")
    for pair in zip(ids, labels):
        f.write(str(pair[0]) + "\t" + str(pair[1]) + "\n")
    f.close()

def tester(train_file):
    train_data = parser_train_data(train_file = train_file)
    train_features = [{"gender": k.get("gender"),
                       "age": k.get("age"),
                       "os": k.get("os"),
                       "browser": k.get("browser"),
                       "domain": k.get("domain") }  for  k in train_data ]   
    train_labels = [ k.get("label")   for  k in train_data ]
    vec = DictVectorizer()
    matr = vec.fit_transform(train_features).toarray()
    n = test_classifier(train_features = matr, target = train_labels)
    print n
    return n

def predicter(train_file, predict_file, res_file):     
    train_data = parser_train_data(train_file = train_file)
    predict_data = parser_predict_data(predict_file = predict_file)
    train_features = [{"gender": k.get("gender"),
                       "age": k.get("age"),
                       "os": k.get("os"),
                       "browser": k.get("browser"),
                       "domain": k.get("domain") }  for  k in train_data ]
    train_labels = [ k.get("label")   for  k in train_data ]
    predict_features = [{"gender": k.get("gender"),
                         "age": k.get("age"),
                         "os": k.get("os"),
                         "browser": k.get("browser") }  for  k in predict_data ]
    vec = DictVectorizer()                     
    m_feat = vec.fit_transform(train_features + predict_features).toarray()
    m_feat_train = m_feat[:len(train_features)]
    m_feat_predict = m_feat[len(train_features):]
    clf = learn_classifier(m_feat_train, train_labels)
    fin_labels = classify(clf, m_feat_predict)
    save_results(ids = [k.get("id") for k in predict_data], labels = fin_labels, res_file = res_file)                


        
def main():
    train_file = "train_file.txt"
    predict_file = "predict_file.txt"
    res_file  = "res_file.txt"
    #tester(train_file = train_file)
    predicter(train_file = train_file, predict_file = predict_file, res_file = res_file)
     
    
if __name__ == "__main__":
	main()     
    
    
