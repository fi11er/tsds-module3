from sklearn.feature_extraction import DictVectorizer
from sklearn.ensemble import *
from sklearn.cross_validation import train_test_split
from sklearn.naive_bayes import GaussianNB

__author__ = "Oleg Ovlasyuk"

def get_known_data(file):
	f = open(file, 'r+')
	ids = []
	data = []
	labels = []
	for line in f:
		if line[-1] == '\n':
			line = line[:-1]
		sample = line.split('\t')
		key = eval(sample[0])
		ids.append(key[0])
		labels.append(key[1])
		data.append(eval(sample[1]))
	f.close()
	return ids, data, labels

def get_unknown_data(file):
	f = open(file, 'r+')
	ids = []
	data = []
	for line in f:
		if line[-1] == '\n':
			line = line[:-1]
		sample = line.split('\t')
		ids.append(eval(sample[0]))
		data.append(eval(sample[1]))
	f.close()
	return ids, data

def save_data(ids, labels, file):
	f = open(file, 'w+')
	for k in zip(ids, labels):
		f.write(str(k[0]) + '\t' + str(k[1]) + '\n')
	f.close()

def vectorize(measurements):
	vec = DictVectorizer()
	return vec.fit_transform(measurements).toarray()

def get_classifier(train_samples, train_labels):
	return RandomForestClassifier(verbose=1).fit(train_samples, train_labels)

def get_score(clf, samples, labels):
	res_labels = clf.predict(samples)
	true = 0;
	ln = 0;
	for k in zip(res_labels, labels):
		if k[1] == '2':
			continue
		if k[0] == k[1]:
			true += 1
		ln += 1
	return float(true) / ln


def test_classifier(samples, labels):
	print "split samples"
	X_train, X_test, y_train, y_test = train_test_split(
		samples, labels, test_size=0.3, random_state=0)
	print "start fit"
	clf = get_classifier(X_train, y_train)
	print 'get score' 
	return get_score(clf, X_test, y_test)


def predict(clf, predict_samples):
	return clf.predict(predict_samples)

def tester(train_file):
	print "tester start"
	train_ids, train_data, train_labels = get_known_data(train_file)
	train_samples = vectorize(train_data)
	score = test_classifier(train_samples, train_labels)
	print "score: ", score

def predictor(train_file, predict_file, out_file):
	train_ids, train_data, train_labels = get_known_data(train_file)
	predict_ids, predict_data = get_unknown_data(predict_file)
	all_samples = vectorize(train_data + predict_data)
	train_samples = all_samples[:len(train_data)]
	predict_samples = all_samples[len(train_data):]

	clf = get_classifier(train_samples, train_labels)
	predict_labels = predict(clf, predict_samples)
	save_data(train_ids, predict_labels, out_file)


def main():
	train_file = "train_data.txt"
	predict_file = "predict_data.txt"
	out_file = "predicted.txt"
	#tester(train_file)
	predictor(train_file, predict_file, out_file)

if __name__ == "__main__":
	main()

