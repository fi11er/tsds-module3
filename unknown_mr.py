#!/usr/bin/python
import urlparse

class Mapper:

    def __init__(self):
        with open("tv_unknown.txt", "rU") as ids_file:
            self.ids = set((line.strip().lower() for line in ids_file))

    def __call__(self, key, value):
        items = value.split("\t")
        user_id = items[0]
        gender = items[2]
        if gender == 'None':
            gender = None
        else:
            gender = int(gender)
        age = items[3]
        if age == 'None':
            age = None
        else:
            age = int(age)
        os = items[4].split('/')[0]
        if os == 'None':
            os = None
        browser = items[5].split('/')[0]
        if browser == 'None':
            browser = None
        domain = urlparse.urlparse(items[8]).netloc
        if (user_id in self.ids) and (age is not None) and (gender is not None) and (os is not None) and (browser is not None):
            yield user_id, (gender, age, os, browser, domain)


def reducer(key, values):
    values = [item for item in values]

    vals = []
    for index in range(5):
        lst = [item[index] for item in values if item is not None]
        vals.append(max(set(lst), key=lst.count))

    yield key, dict(enumerate(vals))


if __name__ == "__main__":
    import dumbo
    dumbo.run(Mapper, reducer, combiner=reducer)
